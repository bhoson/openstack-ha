# HA arch Pre-requisites
```sh
1) change IPs in inventory file
```
```sh
2) change IPs in group_vars/ha-mode 
```

Things to-do before execution:
  - Initially in roles/HAarch/tasks/main.yml, only networksetup.yml is active and the other  ".yml"  entries are commented (i.e. inactive) 
  - First execute ./run.bash with this config to setup the networking on the nodes.
  - After this, SSH to the nodes and do service network restart and ensure that the provider NIC has become unnumbered.
  - Now, uncomment all other entries in roles/HAarch/tasks/main.yml and comment networksetup.yml.
  - Execute ./run.bash

```sh
3) There is no step three.
```
