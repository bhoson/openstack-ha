#!/bin/bash
stat=$([[ $(cat /sys/class/net/{{ controller2_provider_nic }}/operstate) -eq "up" ]] && echo "up" || echo "down")
while [ $stat == "up" ];do
  $(service network restart)
  stat=$([[ $(cat /sys/class/net/{{ controller2_provider_nic }}/operstate) -eq "up" ]] && echo "up" || echo "down")
done
